let to = document.getElementById('userPhone').innerHTML;
const paymentForm = document.getElementById('paymentForm');
paymentForm.addEventListener("submit", payWithPaystack, false);
function payWithPaystack(e) {
  e.preventDefault();
  let handler = PaystackPop.setup({
    key: 'pk_test_71dba9cdabea8f77f5c89424325140d7d6aa50e4', // Replace with your public key
    email: document.getElementById("payEmail").value,
    amount: document.querySelector('#payAmount').value * 100,
    firstname: document.getElementById("userName").innerHTML,
    metadata:{
        fullName: document.getElementById("userName").innerHTML,
        invoice: document.getElementById("payInvoice").value
    },
    // lastname: document.getElementById("userName").innerHTML,
    ref: ''+Math.floor((Math.random() * 1000000000) + 1), 
  
    onClose: function(){
      alert('Window closed.');
    },
    callback: function(response){
            $.ajax({
            url: "https://afx-college.herokuapp.com/apply/pay/verify?reference=" + response.reference,
            method: 'get',
            success: function (response) {
              
              axios({
                    method: 'POST',
                    header: {
                        crossDomain:"true",
                        "Access-Control-Allow-Origin": "*",
                    },
                    url: 'https://www.bulksmsnigeria.com/api/v1/sms/create',
                    data:{
                        "api_token" : "xNw3PxnUtYbHDlPQxgwiQufaswSIMrLDfuApdCAlTtN6yXUgUlyYKU2TUUH0",
                        "from": "AFS Academy",
                        "to": to,
                        "body": "Thank you for your successful registration. Your admission letter will be ready within 48hours. Kindly check your mail or call +2348076974141 for more enquiries.",
                    },
                    })
                    .then(function (response) {
                    console.log(response)
                    }).catch(function (error) {
                    window.location.reload(true)
                    // return Promise.reject(error.response)
                    });
                }

            });

    }
    
  });
  handler.openIframe();

}

    
      